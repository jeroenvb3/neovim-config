local M = {}

local map = vim.keymap.set

map("n", "<leader>tn", "<cmd>tabnew<CR><cmd> Telescope find_files <CR>", { desc = "New Tab and Search"})
map("n", "<leader>tl", "<cmd>tabnext<CR>", { desc = "Next Tab"})
map("n", "<leader>th", "<cmd>tabnext<CR>", { desc = "Previous Tab"})

map("n", "<leader>fv", "<cmd>vsplit<CR><cmd> Telescope find_files <CR>", { desc = "Vertical split and Search"})
map("n", "<leader>fh", "<cmd>split<CR><cmd> Telescope find_files <CR>", { desc = "Horizontal split and Search"})

map("n", "<leader>y", '"+y', { desc = "Copy to clipboard", noremap = true, silent = true})
map("v", "<leader>y", '"+y', { desc = "Copy to clipboard", noremap = true, silent = true})
